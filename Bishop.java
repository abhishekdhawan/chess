public class Bishop extends Piece {

	public Bishop(int a, int b, String c, Board yolo) {
		x = a;
		y = b;
		colour = c;
		type = "Bishop";
		this.b = yolo;
	}
	
	public boolean checkPath(int x, int y) {
		int c, d;
		if (x > this.x) {
			c = 1;
		} else {
			c = -1;
		}
		if (y > this.y) {
			d = 1;
		} else {
			d = -1;
		}
		for (int i = this.x + c, j = this.y + d; i != x; i += c, j += d) {
			if (b.pieceAt(i, j) != null) {
				return false;
			}
		}
		return true;
	}
	
}