public class Pawn extends Piece {

	public Pawn(int a, int b, String c, Board yolo) {
		x = a;
		y = b;
		colour = c;
		type = "Pawn";
		this.b = yolo;
	}
	
	public boolean checkPath(int x, int y) {
		if (colour.equals("Black")) {
			if (this.y - y == 1) {
				if (Math.abs(this.x - x) == 1) {
					return b.pieceAt(x, y) != null;
				} else if (this.x == x) {
					return b.pieceAt(x, y) == null;
				}
			} else if (this.y - y == 2) {
				return this.y == 6;
			}
		} else {
			if (y - this.y == 1) {
				if (Math.abs(this.x - x) == 1) {
					return b.pieceAt(x, y) != null;
				} else if (this.x == x) {
					return b.pieceAt(x, y) == null;
				}
			} else if (y - this.y == 2) {
				return this.y == 1;
			}
		}
		return false;
	}
	
}
