public class Rook extends Piece {

	public Rook(int a, int b, String c, Board yolo) {
		x = a;
		y = b;
		colour = c;
		type = "Rook";
		this.b = yolo;
	}
	
	public boolean checkPath(int x, int y) {
		if (x != this.x && y != this.y) {
			return false;
		} else if (x != this.x) {
			int c;
			if (x > this.x) {
				c = 1;
			} else {
				c = -1;
			}
			for (int i = this.x + c; i != x; i += c) {
				if (b.pieceAt(i, y) != null) {
					return false;
				}
			}
		} else {
			int c;
			if (y > this.y) {
				c = 1;
			} else {
				c = -1;
			}
			for (int i = this.y + c; i != y; i += c) {
				if (b.pieceAt(x, i) != null) {
					return false;
				}
			}
		}
		return true;
	}
	
}