public abstract class Piece {

	protected int x, y;
	protected String colour, type;
	protected Board b;

	public void makeMove(int x, int y) {
		if (b.pieceAt(x, y) != null) {
			b.remove(x, y);
		}
		b.place(b.remove(this.x, this.y), x, y);
		this.x = x;
		this.y = y;
	}

	public String type() {
		return type;
	}

	public String colour() {
		return colour;
	}

	public abstract boolean checkPath(int x, int y);
	
}