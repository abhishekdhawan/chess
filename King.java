public class King extends Piece {

	public King(int a, int b, String c, Board yolo) {
		x = a;
		y = b;
		colour = c;
		type = "King";
		this.b = yolo;
	}

	public boolean check(int x, int y) {
		if (x > 7 || x < 0 || y > 7 || y < 0) {
			return true;
		}
		int i, j;
		for (i = x + 1, j = y + 1; i < 8 && j < 8; i += 1, j += 1) {
			Piece p = b.pieceAt(i, j);
			if (p != null && !p.colour().equals(colour)) {
				if (p.type().equals("Queen") || p.type().equals("Bishop") || Math.abs(i - x) == 1 && p.type().equals("Pawn") && colour.equals("White")) {
					return true;
				} else {
					break;
				}
			}
		}
		for (i = x + 1, j = y - 1; i < 8 && j >= 0; i += 1, j -= 1) {
			Piece p = b.pieceAt(i, j);
			if (p != null && !p.colour().equals(colour)) {
				if (p.type().equals("Queen") || p.type().equals("Bishop") || Math.abs(i - x) == 1 && p.type().equals("Pawn") && colour.equals("Black")) {
					return true;
				} else {
					break;
				}
			}
		}
		for (i = x - 1, j = y + 1; i >= 0 && j < 8; i -= 1, j += 1) {
			Piece p = b.pieceAt(i, j);
			if (p != null && !p.colour().equals(colour)) {
				if (p.type().equals("Queen") || p.type().equals("Bishop") || Math.abs(i - x) == 1 && p.type().equals("Pawn") && colour.equals("White")) {
					return true;
				} else {
					break;
				}
			}
		}
		for (i = x - 1, j = y - 1; i >= 0 && j >= 0; i -= 1, j -= 1) {
			Piece p = b.pieceAt(i, j);
			if (p != null && !p.colour().equals(colour)) {
				if (p.type().equals("Queen") || p.type().equals("Bishop") || Math.abs(i - x) == 1 && p.type().equals("Pawn") && colour.equals("Black")) {
					return true;
				} else {
					break;
				}
			}
		}
		for (i = x + 1; i < 8; i += 1) {
			Piece p = b.pieceAt(i, y);
			if (p != null && !p.colour().equals(colour)) {
				if (p.type().equals("Queen") || p.type().equals("Rook")) {
					return true;
				} else {
					break;
				}
			}
		}
		for (i = x - 1; i >= 0; i -= 1) {
			Piece p = b.pieceAt(i, y);
			if (p != null && !p.colour().equals(colour)) {
				if (p.type().equals("Queen") || p.type().equals("Rook")) {
					return true;
				} else {
					break;
				}
			}
		}
		for (i = y + 1; i < 8; i += 1) {
			Piece p = b.pieceAt(x, i);
			if (p != null && !p.colour().equals(colour)) {
				if (p.type().equals("Queen") || p.type().equals("Rook")) {
					return true;
				} else {
					break;
				}
			}
		}
		for (i = y - 1; i >= 0; i -= 1) {
			Piece p = b.pieceAt(x, i);
			if (p != null && !p.colour().equals(colour)) {
				if (p.type().equals("Queen") || p.type().equals("Rook")) {
					return true;
				} else {
					break;
				}
			}
		}
		if (b.pieceAt(x + 2, y + 1) != null && b.pieceAt(x + 2, y + 1).type().equals("Knight") || b.pieceAt(x + 2, y - 1) != null && b.pieceAt(x + 2, y - 1).type().equals("Knight") ||
				b.pieceAt(x - 2, y + 1) != null && b.pieceAt(x - 2, y + 1).type().equals("Knight") ||b.pieceAt(x - 2, y - 1) != null && b.pieceAt(x - 2, y - 1).type().equals("Knight") ||
				b.pieceAt(x + 1, y + 2) != null && b.pieceAt(x + 1, y + 2).type().equals("Knight") ||b.pieceAt(x - 1, y + 2) != null && b.pieceAt(x - 1, y + 2).type().equals("Knight") ||
				b.pieceAt(x + 1, y - 2) != null && b.pieceAt(x + 1, y - 2).type().equals("Knight") ||b.pieceAt(x - 1, y - 2) != null && b.pieceAt(x - 1, y - 2).type().equals("Knight")) {
			return true;
		}
		return false;
	}
	
	public boolean checkMate() {
		if (!check(x, y)) {
			return false;
		}
		Piece p = b.pieceAt(x + 1, y + 1);
		if ((p == null || !p.colour().equals(colour)) && !check(x + 1, y + 1)) {
			return false;
		}
		p = b.pieceAt(x + 1, y - 1);
		if ((p == null || !p.colour().equals(colour)) && !check(x + 1, y - 1)) {
			return false;
		}
		p = b.pieceAt(x - 1, y + 1);
		if ((p == null || !p.colour().equals(colour)) && !check(x - 1, y + 1)) {
			return false;
		}
		p = b.pieceAt(x - 1, y - 1);
		if ((p == null || !p.colour().equals(colour)) && !check(x - 1, y - 1)) {
			return false;
		}
		p = b.pieceAt(x, y + 1);
		if ((p == null || !p.colour().equals(colour)) && !check(x, y + 1)) {
			return false;
		}
		p = b.pieceAt(x + 1, y);
		if ((p == null || !p.colour().equals(colour)) && !check(x + 1, y)) {
			return false;
		}
		p = b.pieceAt(x, y - 1);
		if ((p == null || !p.colour().equals(colour)) && !check(x, y - 1)) {
			return false;
		}
		p = b.pieceAt(x - 1, y);
		if ((p == null || !p.colour().equals(colour)) && !check(x - 1, y)) {
			return false;
		}
		return true;
	}

	public boolean checkPath(int x, int y) {
		int a = Math.abs(x - this.x);
		int b = Math.abs(y - this.y);
		return a == 1 && b == 1 && !check(x, y) && (this.b.pieceAt(x, y) == null || !this.b.pieceAt(x, y).colour().equals(colour));
	}
	
}