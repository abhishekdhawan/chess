public class Queen extends Piece {

	public Queen(int a, int b, String c, Board yolo) {
		x = a;
		y = b;
		colour = c;
		type = "Queen";
		this.b = yolo;
	}
	
	public boolean checkBPath(int x, int y) {
		int c, d;
		if (x > this.x) {
			c = 1;
		} else {
			c = -1;
		}
		if (y > this.y) {
			d = 1;
		} else {
			d = -1;
		}
		for (int i = this.x + c, j = this.y + d; i != x; i += c, j += d) {
			if (b.pieceAt(i, j) != null) {
				return false;
			}
		}
		return true;
	}

	public boolean checkPath(int x, int y) {
		if (checkBPath(x, y)) {
			return true;
		}
		if (x != this.x && y != this.y) {
			return false;
		} else if (x != this.x) {
			int c;
			if (x > this.x) {
				c = 1;
			} else {
				c = -1;
			}
			for (int i = this.x + c; i != x; i += c) {
				if (b.pieceAt(i, y) != null) {
					return false;
				}
			}
		} else {
			int c;
			if (y > this.y) {
				c = 1;
			} else {
				c = -1;
			}
			for (int i = this.y + c; i != y; i += c) {
				if (b.pieceAt(x, i) != null) {
					return false;
				}
			}
		}
		return true;
	}
	
}