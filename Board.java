public class Board {

	private Piece array[][], selected;
	private King wK, bK;
	private boolean madeMove;
	private String player;

	public static void main(String[] args) {
		StdDrawPlus.setXscale(0, 8);
        StdDrawPlus.setYscale(0, 8);
        Board game = new Board();
		game.drawBoard();
		while (true) {
			game.drawBoard();
			if (StdDrawPlus.mousePressed()) {
				double x = StdDrawPlus.mouseX();
				double y = StdDrawPlus.mouseY();
				if (game.canSelect((int)x, (int)y)) {
					game.select((int)x, (int)y);
					StdDrawPlus.setPenColor(StdDrawPlus.WHITE);
					StdDrawPlus.filledSquare((int)x + .5, (int)y + .5, .5);
				}
			}
			if (StdDrawPlus.isSpacePressed())	
    	       	if (game.canEndTurn())
	           		game.endTurn();
			StdDrawPlus.show(100);
			if (game.winner() != null) {
				System.out.println(game.winner().toUpperCase() + " WINS!!!!!");
				game.drawBoard();
				return;
			}
		}
	}

	public Board() {
		array = new Piece[8][8];
		selected = null;
		madeMove = false;
		player = "White";
		for (int i = 0 ; i < 8; i += 1) {
			place(new Pawn(i, 1, "White", this), i, 1);
			place(new Pawn(i, 6, "Black", this), i, 6);
		}
		place(new Rook(0, 0, "White", this), 0, 0);
		place(new Rook(7, 0, "White", this), 7, 0);
		place(new Rook(0, 7, "Black", this), 0, 7);
		place(new Rook(7, 7, "Black", this), 7, 7);
		place(new Knight(1, 0, "White", this), 1, 0);
		place(new Knight(6, 0, "White", this), 6, 0);
		place(new Knight(1, 7, "Black", this), 1, 7);
		place(new Knight(6, 7, "Black", this), 6, 7);
		place(new Bishop(2, 0, "White", this), 2, 0);
		place(new Bishop(5, 0, "White", this), 5, 0);
		place(new Bishop(2, 7, "Black", this), 2, 7);
		place(new Bishop(5, 7, "Black", this), 5, 7);
		place(new Queen(3, 0, "White", this), 3, 0);
		place(new King(4, 0, "White", this), 4, 0);
		place(new Queen(3, 7, "Black", this), 3, 7);
		place(new King(4, 7, "Black", this), 4, 7);
		wK = (King) pieceAt(4, 0);
		bK = (King) pieceAt(4, 7);
	}

	private void drawBoard() {
		int i, j;
		String img;
		for (j = 0; j < 8; j += 1)
			for (i = 0; i < 8; i += 1) {
				if ((i + j) % 2 == 0)
					StdDrawPlus.setPenColor(StdDrawPlus.BLACK);
				else
					StdDrawPlus.setPenColor(StdDrawPlus.WHITE);
				StdDrawPlus.filledSquare(i + 0.5, j + 0.5, 0.5);
				img = "img/";
				Piece temp = pieceAt(i, j);
				if (temp != null) {
					img = img.concat(temp.colour());
					img = img.concat("-");
					img = img.concat(temp.type());
					img = img.concat(".png");
					StdDrawPlus.picture(i + .5, j + .5, img, 1, 1);
				}
			}
	}

	public Piece pieceAt(int x, int y) {
		if (x > 7 || y > 7 || x < 0 || y < 0) {
			return null;
		}
		return array[x][y];
	}

	public boolean canSelect(int x, int y) {
		Piece p = pieceAt(x, y);
		if (selected != null) {
			if (p == null || !p.colour().equals(selected.colour)) {
				return selected.checkPath(x, y);
			} else if (p.colour().equals(selected.colour) && !madeMove) {
				return true;
			}
			return false;
		}
		return p != null && p.colour().equals(player);
	}

	public void select(int x, int y) {
		Piece p = pieceAt(x, y);
		if (selected == null) {
			selected = p;
		} else {
			if (p != null && selected.colour().equals(p.colour())) {
				selected = p;
			} else {
				selected.makeMove(x, y);
				madeMove = true;
			}
		}
	}

	public void place(Piece p, int x, int y) {
		array[x][y] = p;
	}

	public Piece remove(int x, int y) {
		Piece p = pieceAt(x, y);
		array[x][y] = null;
		return p;
	}

	public boolean canEndTurn() {
		return madeMove;
	}

	public void endTurn() {
		if (player.equals("White")) {
			player = "Black";
		} else {
			player = "White";
		}
		madeMove = false;
		selected = null;
	}

	public String winner() {
		if (wK.checkMate()) {
			return "Black";
		} else if (bK.checkMate()) {
			return "White";
		}
		return null;
	}
	
}