public class Knight extends Piece {

	public Knight(int a, int b, String c, Board yolo) {
		x = a;
		y = b;
		colour = c;
		type = "Knight";
		this.b = yolo;
	}
	
	public boolean checkPath(int x, int y) {
		int a = Math.abs(x - this.x);
		int b = Math.abs(y - this.y);
		if (a == 2 && b == 1 || a == 1 && b == 2) {
			return true;
		}
		return false;
	}
	
}